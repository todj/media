# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gsettings meson

SUMMARY="A set of plugins that need more quality"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    gobject-introspection
    gstreamer_plugins:
        ass          [[ description = [ ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle rendering using libass ] ]]
        bluez        [[ description = [ Support streaming media over Bluetooth using bluez ] ]]
        bs2b         [[ description = [ Improve headphone listening of stereo audio using the bs2b library ] ]]
        chromaprint  [[ description = [ Audio fingerprinting using the Chromaprint library ] ]]
        closedcaption [[ description = [ Closed caption extractor, decoder, and overlay plugin ] ]]
        curl         [[ description = [ Support uploading data to a server using curl ] ]]
        dash         [[ description = [ Support Dynamic Adaptive Streaming over HTTP (DASH) ] ]]
        dc1394       [[ description = [ Support for getting data from IIDC FireWire cameras using libdc1394 ] ]]
        dtls         [[ description = [ Datagram Transport Layer Security (DTLS) decoder and encoder plugins ] ]]
        dts          [[ description = [ DTS (multichannel digital surround sound) audio decoding using libdca ] ]]
        faac         [[ description = [ AAC audio encoding using faac ] ]]
        faad         [[ description = [ MPEG-2/4 AAC audio decoding using faad2 ] ]]
        fdk-aac      [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library ] ]]
        flite        [[ description = [ Support for Flite speech synthesizer ] ]]
        fluidsynth   [[ description = [ Support for Fluidsynth MIDI decoder ] ]]
        gme          [[ description = [ Support for libgme gaming console music file decoder ] ]]
        gsm          [[ description = [ GSM audio decoding and encoding ] ]]
        hdr          [[ description = [ OpenEXR plugin for decoding high-dynamic-range EXR images ] ]]
        jpeg2000     [[ description = [ JPEG 2000 image compression and decompression using OpenJPEG ] ]]
        kate         [[ description = [ Kate (karaoke and text codec) stream encoding/decoding using libkate and rendering using libtiger ] ]]
        kms          [[ description = [ Enable KMS video sink ] ]]
        ladspa       [[ description = [ Linux Audio Developer Simple Plugin API support ] ]]
        lcms         [[ description = [ ICC color correction support ] ]]
        libde265     [[ description = [ HEVC/H.265 video encoder using libde265 ] ]]
        libmms       [[ description = [ Support for mms:// and mmsh:// network streams ] ]]
        lv2          [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
        modplug      [[ description = [ Support for ModPlug audio decoder ] ]]
        mjpeg        [[ description = [ MPEG-1/2 video encoding and MPEG/DVD/SVCD/VCD video/audio multiplexing using mjpegtools ] ]]
        musepack     [[ description = [ Musepack audio decoding using libmpcdec ] ]]
        neon         [[ description = [ HTTP source handling using neon ] ]]
        ofa          [[ description = [ MusicIP audio fingerprint calculation using libofa ] ]]
        openal       [[ description = [ OpenAL sink ] ]]
        opencv       [[ description = [ Computer Vision techniques from OpenCV ] ]]
        openh264     [[ description = [ H264 video decoding using OpenH264 ] ]]
        opus         [[ description = [ Enable Opus parser plugin ] ]]
        rtmp         [[ description = [ Support for rtmp:// and rtmpe:// network streams ] ]]
        resin        [[ description = [ DVD playback plugin ] ]]
        sbc          [[ description = [ SBC bluetooth audio codec plugin ]
                        requires = [ gstreamer_plugins: bluez ] ]]
        sndfile      [[ description = [ Plugin to write and read various audio formats using libsndfile ] ]]
        soundtouch   [[ description = [ BPM detection and audio pitch controlling using soundtouch ] ]]
        srtp         [[ description = [ SRTP (encrypted RTP) support ] ]]
        svg          [[ description = [ SVG image decoding using librsvg ] ]]
        teletext     [[ description = [ Support Teletext subtitles ] ]]
        ttml         [[ description = [ Support TTML subtitles ] ]]
        uvc          [[ description = [ UVC Video Camera H.264 support ] ]]
        v4l          [[ description = [ V4L2 CODECs Accelerator support ] ]]
        va           [[ description = [ Enable support for decoding video using the Video Acceleration API ] ]]
        vo-aacenc    [[ description = [ AAC encoder using vo-aacenc ] ]]
        vo-amrwbenc  [[ description = [ OpenCORE AMR-WB decoder and encoder (audio) ] ]]
        vulkan       [[ description = [ Vulkan video output support ] ]]
        wayland      [[ description = [ wayland client video renderer ]
                        requires = [ gstreamer_plugins: kms ] ]]
        webp         [[ description = [ Support WebP image decoding using libwebp ] ]]
        webrtc       [[ description = [ WebRTC support ] ]]
        webrtcdsp    [[ description = [ Audio processing and echo cancellation using webrtc-audio-processing ] ]]
        wildmidi     [[ description = [ Support for WildMidi midi soft synth ] ]]
        X            [[ description = [ X11 client video renderer ] ]]
        x265         [[ description = [ HEVC/H.265 video encoder using x265 ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers:
        libgcrypt [[ description = [ Use libgcrypt as the crypto provider ] ]]
        libressl  [[ description = [ Use LibreSSL as the crypto provider ] ]]
        nettle    [[ description = [ Use nettle as the crypto provider ] ]]
        openssl   [[ description = [ Use OpenSSL as the crypto provider ] ]]
    ) [[
        number-selected = exactly-one
    ]]
    gstreamer_plugins:dtls? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    gstreamer_plugins:vulkan? ( ( gstreamer_plugins: X wayland ) [[ number-selected = at-least-one ]] )
    ( gstreamer_plugins: libde265 x265 ) [[
        number-selected = at-most-one
        *description = [ HEVC/H.265 provider ]
    ]]
"

DEPENDENCIES="
    build:
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
    test:
        media-libs/libexif[>=0.6.16]
    build+run:
        app-arch/bzip2
        dev-libs/glib:2[>=2.44.0]
        dev-libs/orc:0.4[>=0.4.17]
        media-libs/gstreamer:1.0[>=${PV}]
        media-libs/libpng:=[>=1.0]
        media-plugins/gst-plugins-base:1.0[>=${PV}][X?][wayland?]
        gstreamer_plugins:ass? ( media-libs/libass[>=0.10.2] )
        gstreamer_plugins:bluez? (
            net-wireless/bluez[>=5.0]
            sys-apps/dbus
        )
        gstreamer_plugins:bs2b? ( media-libs/libbs2b[>=3.1.0] )
        gstreamer_plugins:chromaprint? ( media-libs/chromaprint )
        gstreamer_plugins:closedcaption? ( x11-libs/pango[>=1.22.0] )
        gstreamer_plugins:curl? (
            net-misc/curl[>=7.55.0]
            net-libs/libssh2[>=1.4.3]
        )
        gstreamer_plugins:dash? ( dev-libs/libxml2:2.0[>=2.8] )
        gstreamer_plugins:dc1394? ( media-libs/libdc1394:2[>=2.2.5] )
        gstreamer_plugins:dtls? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl:=[>=1.0.1] )
        )
        gstreamer_plugins:dts? ( media-libs/libdca )
        gstreamer_plugins:faac? ( media-libs/faac )
        gstreamer_plugins:faad? ( media-libs/faad2[>=2.7] )
        gstreamer_plugins:fdk-aac? ( media-libs/fdk-aac[>=0.1.4] )
        gstreamer_plugins:flite? ( app-speech/flite )
        gstreamer_plugins:fluidsynth? ( media-sound/fluidsynth[>=1.0] )
        gstreamer_plugins:gme? ( media-libs/libgme )
        gstreamer_plugins:gsm? ( media-libs/gsm )
        gstreamer_plugins:hdr? ( media-libs/openexr )
        gstreamer_plugins:jpeg2000? ( media-libs/OpenJPEG:2[>=2.2] )
        gstreamer_plugins:kate? ( media-libs/libkate[>=0.1.7] )
        gstreamer_plugins:kms? ( x11-dri/libdrm[>=2.4.98] )
        gstreamer_plugins:ladspa? (
            media-libs/ladspa-sdk
            media-libs/liblrdf
        )
        gstreamer_plugins:lcms? ( media-libs/lcms2[>=2.7] )
        gstreamer_plugins:libde265? ( media-libs/libde265[>=0.9] )
        gstreamer_plugins:libmms? ( media-libs/libmms[>=0.4] )
        gstreamer_plugins:lv2? ( media-libs/lilv[>=0.22] )
        gstreamer_plugins:modplug? ( media-libs/libmodplug )
        gstreamer_plugins:musepack? ( media-libs/libmpcdec )
        gstreamer_plugins:mjpeg? ( media-video/mjpegtools[>=2.0.0] )
        gstreamer_plugins:neon? ( net-misc/neon[>=0.27.0&<=0.31.99] )
        gstreamer_plugins:ofa? ( media-libs/libofa[>=0.9.3] )
        gstreamer_plugins:openal? ( media-libs/openal[>=1.14] )
        gstreamer_plugins:opencv? ( media-libs/opencv[>=3.0.0&<=4.6.0][contrib_modules:bgsegm] )
        gstreamer_plugins:openh264? ( media-libs/openh264[>=1.3] )
        gstreamer_plugins:opus? ( media-libs/opus[>=0.9.4] )
        gstreamer_plugins:resin? (
            media-libs/libdvdnav[>=4.1.2]
            media-libs/libdvdread[>=4.1.2]
        )
        gstreamer_plugins:rtmp? ( media-video/rtmpdump )
        gstreamer_plugins:sbc? ( media-libs/sbc[>=1.0] )
        gstreamer_plugins:sndfile? ( media-libs/libsndfile[>=1.0.16] )
        gstreamer_plugins:soundtouch? ( media-libs/soundtouch[>=1.4] )
        gstreamer_plugins:srtp? ( net-libs/libsrtp[>=2.1.0] )
        gstreamer_plugins:svg? (
            gnome-desktop/librsvg:2[>=2.36.2]
            x11-libs/cairo[>=1.0]
        )
        gstreamer_plugins:teletext? (
            media-libs/zvbi[>=0.2]
        )
        gstreamer_plugins:ttml? (
            dev-libs/libxml2:2.0[>=2.9.2]
            x11-libs/cairo
            x11-libs/pango
        )
        gstreamer_plugins:uvc? (
            dev-libs/libusb:1
            gnome-desktop/libgudev
        )
        gstreamer_plugins:v4l? ( gnome-desktop/libgudev )
        gstreamer_plugins:va? (
            gnome-desktop/libgudev
            x11-dri/libdrm
            x11-libs/libva[>=1.6]
        )
        gstreamer_plugins:vo-aacenc? ( media-libs/vo-aacenc[>=0.1.0] )
        gstreamer_plugins:vo-amrwbenc? ( media-libs/vo-amrwbenc[>=0.1.0] )
        gstreamer_plugins:vulkan? (
            sys-libs/vulkan-headers
            sys-libs/vulkan-loader
            gstreamer_plugins:X? ( x11-libs/libxkbcommon )
        )
        gstreamer_plugins:wayland? (
            sys-libs/wayland[>=1.15]
            sys-libs/wayland-protocols[>=1.15]
            x11-dri/libdrm[>=2.4.55]
        )
        gstreamer_plugins:webp? ( media-libs/libwebp:=[>=0.2.1] )
        gstreamer_plugins:webrtc? ( net-im/libnice[>=0.1.14] )
        gstreamer_plugins:webrtcdsp? ( media-libs/webrtc-audio-processing[>=0.2&<0.4] )
        gstreamer_plugins:wildmidi? ( media-libs/wildmidi )
        gstreamer_plugins:X? (
            x11-libs/libX11
            x11-libs/libxcb[>=1.10]
        )
        gstreamer_plugins:x265? ( media-libs/x265:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libgcrypt? ( dev-libs/libgcrypt )
        providers:libressl? ( dev-libs/libressl:= )
        providers:nettle? ( dev-libs/nettle:=[>=3.0] )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.18.0-opencv-prefix.patch
    "${FILES}"/${PN}-1.18.3-gst-don-t-use-volatile-to-mean-atomic.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=disabled

    # core plugins
    -Daccurip=enabled
    -Dadpcmdec=enabled
    -Dadpcmenc=enabled
    -Daiff=enabled
    -Dasfmux=enabled
    -Daudiobuffersplit=enabled
    -Daudiofxbad=enabled
    -Daudiolatency=enabled
    -Daudiomixmatrix=enabled
    -Daudiovisualizers=enabled
    -Dautoconvert=enabled
    -Dbayer=enabled
    -Dcamerabin2=enabled
    -Dcoloreffects=enabled
    -Ddebugutils=enabled
    -Ddvbsubenc=enabled
    -Ddvbsuboverlay=enabled
    -Ddvdspu=enabled
    -Dfaceoverlay=enabled
    -Dfestival=enabled
    -Dfieldanalysis=enabled
    -Dfreeverb=enabled
    -Dfrei0r=enabled
    -Dgaudieffects=enabled
    -Dgdp=enabled
    -Dgeometrictransform=enabled
    -Did3tag=enabled
    -Dinter=enabled
    -Dinterlace=enabled
    -Divfparse=enabled
    -Divtc=enabled
    -Djp2kdecimator=enabled
    -Djpegformat=enabled
    -Dlibrfb=enabled
    -Dmidi=enabled
    -Dmpegdemux=enabled
    -Dmpegpsmux=enabled
    -Dmpegtsdemux=enabled
    -Dmpegtsmux=enabled
    -Dmxf=enabled
    -Dnetsim=enabled
    -Dorc=enabled
    -Dpcapparse=enabled
    -Dpnm=enabled
    -Dproxy=enabled
    -Drawparse=enabled
    -Dremovesilence=enabled
    -Drist=enabled
    -Drtmp2=enabled
    -Drtp=enabled
    -Dsdp=enabled
    -Dsegmentclip=enabled
    -Dsiren=enabled
    -Dshm=enabled
    -Dsmooth=enabled
    -Dspeed=enabled
    -Dsubenc=enabled
    -Dswitchbin=enabled
    -Dtimecode=enabled
    -Dvideofilters=enabled
    -Dvideoframe_audiolevel=enabled
    -Dvideoparsers=enabled
    -Dvideosignal=enabled
    -Dvmnc=enabled
    -Dy4m=enabled

    -Dbz2=enabled
    -Ddecklink=enabled
    -Ddvb=enabled
    -Dfbdev=enabled
    -Dhls=enabled
    -Dsctp-internal-usrsctp=enabled

    -Dipcpipeline=enabled

    # Windows Specific
    -Dd3dvideosink=disabled
    -Dd3d11=disabled
    -Ddirectsound=disabled
    -Dmediafoundation=disabled
    -Dsmoothstreaming=disabled
    -Dwasapi=disabled
    -Dwasapi2=disabled
    -Dwinks=disabled
    -Dwinscreencap=disabled

    # OS X Specific
    -Dapplemedia=disabled

    # Android specific
    -Dandroidmedia=disabled

    # Unpackaged dependencies
    -Daom=disabled
    -Ddirectfb=disabled
    -Diqa=disabled
    -Dmsdk=disabled
    -Dspandsp=disabled

    -Dopenmpt=disabled
    -Dopenni2=disabled
    -Dopensles=disabled

    -Davtp=disabled
    -Dmicrodns=disabled
    -Dnvcodec=disabled
    -Dsrt=disabled
    -Dsvthevcenc=disabled
    -Dtinyalsa=disabled
    -Dtranscode=disabled
    -Dzbar=disabled
    -Dzxing=disabled
    -Dwpe=disabled
    -Dmagicleap=disabled

    # TODO: Two plugins can use OpenGL: wpe and nvcodec
    -Dgl=disabled

    -Ddoc=disabled
    -Dnls=enabled

    # Disabled debug options
    -Dgobject-cast-checks=disabled
    -Dglib-asserts=disabled
    -Dglib-checks=disabled
    -Dextra-checks=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'

    'gstreamer_plugins:ass assrender'
    'gstreamer_plugins:bluez'
    'gstreamer_plugins:bs2b'
    'gstreamer_plugins:chromaprint'
    'gstreamer_plugins:closedcaption'
    'gstreamer_plugins:curl'
    'gstreamer_plugins:curl curl-ssh2'
    'gstreamer_plugins:dash'
    'gstreamer_plugins:dc1394'
    'gstreamer_plugins:dtls'
    'gstreamer_plugins:dts'
    'gstreamer_plugins:faac'
    'gstreamer_plugins:faad'
    'gstreamer_plugins:fdk-aac fdkaac'
    'gstreamer_plugins:flite'
    'gstreamer_plugins:fluidsynth'
    'gstreamer_plugins:gme'
    'gstreamer_plugins:gsm'
    'gstreamer_plugins:hdr openexr'
    'gstreamer_plugins:jpeg2000 openjpeg'
    'gstreamer_plugins:kate'
    'gstreamer_plugins:kms'
    'gstreamer_plugins:ladspa'
    'gstreamer_plugins:lcms colormanagement'
    'gstreamer_plugins:libde265'
    'gstreamer_plugins:libmms'
    'gstreamer_plugins:lv2'
    'gstreamer_plugins:modplug'
    'gstreamer_plugins:musepack'
    'gstreamer_plugins:mjpeg mpeg2enc'
    'gstreamer_plugins:mjpeg mplex'
    'gstreamer_plugins:neon'
    'gstreamer_plugins:ofa'
    'gstreamer_plugins:openal'
    'gstreamer_plugins:opencv'
    'gstreamer_plugins:openh264'
    'gstreamer_plugins:opus'
    'gstreamer_plugins:resin resindvd'
    'gstreamer_plugins:rtmp'
    'gstreamer_plugins:sbc'
    'gstreamer_plugins:sndfile'
    'gstreamer_plugins:soundtouch'
    'gstreamer_plugins:srtp'
    'gstreamer_plugins:svg rsvg'
    'gstreamer_plugins:teletext'
    'gstreamer_plugins:ttml'
    'gstreamer_plugins:uvc uvch264'
    'gstreamer_plugins:v4l v4l2codecs'
    'gstreamer_plugins:va'
    'gstreamer_plugins:vo-aacenc voaacenc'
    'gstreamer_plugins:vo-amrwbenc voamrwbenc'
    'gstreamer_plugins:vulkan'
    'gstreamer_plugins:wayland'
    'gstreamer_plugins:wildmidi'
    'gstreamer_plugins:webp'
    'gstreamer_plugins:webrtc'
    'gstreamer_plugins:webrtcdsp'
    'gstreamer_plugins:X x11'
    'gstreamer_plugins:x265'
)

src_configure() {
    local crypto=
    if option providers:openssl || option providers:libressl; then
        crypto=openssl
    elif option providers:nettle; then
        crypto=nettle
    else
        crypto=libgcrypt
    fi
    meson_src_configure "-Dhls-crypto=$crypto"
}

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS
    unset DISPLAY
    default
}

