# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require toolchain-funcs

SUMMARY="The Free Lossless Audio Codec"
HOMEPAGE="https://xiph.org/${PN}"
DOWNLOADS="https://downloads.xiph.org/releases/${PN}/${PNV}.tar.xz"

LICENCES="Xiph || ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="ogg"

# All tests are slow, so use only src_test_expensive
RESTRICT="test"

DEPENDENCIES="
    build+run:
        ogg? ( media-libs/libogg )
"

flac-cc_define_enable() {
    cc-has-defined ${1} && echo --enable-${2} || echo --disable-${2}
}

src_configure() {
    local myconf=()

    myconf+=(
        --enable-cpplibs
        --disable-doxygen-docs
        --disable-examples
        --disable-xmms-plugin
        --with-ogg-libraries=/dummy
        $(option_enable ogg)
        # x86
        $(flac-cc_define_enable __AVX__ avx)
        $(flac-cc_define_enable __SSE__ sse)
    )

    econf "${myconf[@]}"
}


src_test_expensive() {
    emake check
}

