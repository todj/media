# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.bz2 ] flag-o-matic

SUMMARY="A library for reading and writing sound files"
DESCRIPTION="
A C library for reading and writing files containing sampled sound (such as MS Windows WAV and the
Apple/SGI AIFF format) through one standard library interface.
"

LICENCES="|| ( LGPL-2.1 LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    alsa   [[ description = [ Use ALSA in example programs ] ]]
    flac   [[ description = [ Support FLAC codec in OGG containers ]
              requires = [ opus vorbis ] ]]
    octave [[ description = [ Build the octave libsndfile module ] ]]
    opus   [[ description = [ Support the Opus audio codec ] ]]
    sqlite [[ description = [ SQLite is only used in regtest ] ]]
    vorbis [[ description = [ Support Vorbis codec in OGG containers ]
              requires = [ flac opus ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        dev-lang/python:*
    build+run:
        alsa? ( sys-sound/alsa-lib )
        flac? (
            media-libs/flac[>=1.3.1]
            media-libs/libogg[>=1.3.0]
        )
        octave? ( sci-apps/octave[=3.0*] )
        opus? ( media-libs/opus[>=1.1] )
        vorbis? (
            media-libs/libvorbis[>=1.2.3]
            media-libs/libogg[>=1.3.0]
        )
    test:
        sqlite? ( dev-db/sqlite:3[>=3.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --htmldir=/usr/share/doc/${PNVR}/html
    --enable-full-suite
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    octave
    sqlite
    "flac external-libs"
    "opus external-libs"
    "vorbis external-libs"
)

src_configure() {
    append-flags -D_FILE_OFFSET_BITS=64

    default
}

