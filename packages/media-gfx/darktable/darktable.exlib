# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=darktable-org release=release-$(ever range -3) suffix=tar.xz ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache \
    option-renames [ renames=[ 'libsecret keyring' ] ] \
    utf8-locale

export_exlib_phases src_prepare src_compile pkg_postinst pkg_postrm

SUMMARY="A virtual lighttable and darkroom for photographers to develop RAW images"
DESCRIPTION="
darktable is an open source photography workflow application and RAW developer.
A virtual lighttable and darkroom for photographers.
It manages your digital negatives in a database, lets you view them through a zoomable lighttable and enables you to develop raw images and enhance them.
"
HOMEPAGE+=" https://www.darktable.org"

LICENCES="
    GPL-3
    MIT [[ note = [ CL ] ]]
    LGPL-2.1 [[ note = [ rawspeed ] ]]
    BSD-2 [[ note = [ LuaAutoC ] ]]
"
SLOT="0"
MYOPTIONS="
    avif [[ description = [ AV1 Image File Format (AVIF) support ] ]]
    camera [[ description = [ Direct camera support via gphoto2, mainly Canon and Nikon ] ]]
    colord [[ description = [ Color management ] ]]
    cups
    geo-mapping [[ description = [ Read and use gps data ] ]]
    hdr [[ description = [ HDR support via openexr ] ]]
    jpeg2000 [[ description = [ JPEG2000 support via openjpeg ] ]]
    keyring [[ description = [ libsecret keyring support ] ]]
    lua [[ description = [ lua scripting support ] ]]
    openmp [[ description = [ Use gcc with openmp support ] ]]
    webp [[ description = [ Support for the WebP image format ] ]]
    ( providers: graphicsmagick imagemagick ) [[
        number-selected = at-most-one
    ]]
"

# TODO:
# Contains a shitload of internal libraries, check if we can get rid of that:
# LuaAutoC (unpackaged)
# CL and OpenCL (headers)
# rawspeed
DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-libs/libxslt [[ note = [ xsltproc is used to generate some headers ] ]]
        sys-devel/gcc[graphite] [[ note = [ fails without ] ]]
        sys-devel/gettext
    build+run:
        app-text/iso-codes[>=3.66]
        core/json-glib
        dev-libs/glib:2[>=2.40]
        dev-libs/libxml2:2.0[>=2.6]
        dev-libs/pugixml[>=1.8]
        dev-db/sqlite:3[>=3.15]
        gnome-desktop/librsvg:2
        gnome-desktop/libsoup:2.4
        graphics/exiv2[>=0.24][xmp(+)]
        media-libs/ExifTool
        media-libs/lcms2
        media-libs/lensfun
        media-libs/libjpeg-turbo
        media-libs/libpng:=
        media-libs/tiff
        net-misc/curl[>=7.56]
        sys-libs/zlib
        x11-libs/cairo
        x11-libs/libX11
        x11-libs/libXrandr
        x11-libs/pango
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.22]
        avif? ( media-libs/libavif:=[>=0.8.2] )
        camera? ( media-libs/libgphoto2 )
        colord? (
            sys-apps/colord[>=0.1.32]
            gnome-desktop/colord-gtk
        )
        cups? ( net-print/cups )
        geo-mapping? ( x11-libs/osm-gps-map )
        hdr? (
            media-libs/ilmbase
            media-libs/openexr
        )
        jpeg2000? ( media-libs/OpenJPEG:2 )
        keyring? ( dev-libs/libsecret:1 )
        lua? ( dev-lang/lua:5.3 )
        openmp? ( sys-libs/libgomp:= )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick )
        providers:imagemagick? ( media-gfx/ImageMagick[>=7] )
        webp? ( media-libs/libwebp:= )
    test:
        dev-util/cmocka[>=1.1.0]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    --hates=CMAKE_BUILD_TYPE
    -DCUSTOM_CFLAGS:BOOL=TRUE
    -DTESTBUILD_OPENCL_PROGRAMS=FALSE
    -DUSE_GAME:BOOL=FALSE
    -DUSE_GMIC:BOOL=FALSE
    -DUSE_KWALLET:BOOL=FALSE
    -DUSE_MAC_INTEGRATION:BOOL=FALSE
    -DUSE_OPENCL:BOOL=TRUE
    -DUSE_UNITY:BOOL=FALSE
    -DUSE_XCF:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'cups PRINT'
)
CMAKE_SRC_CONFIGURE_OPTION_USES=(
    AVIF
    'camera CAMERA_SUPPORT'
    COLORD
    'geo-mapping MAP'
    'hdr OPENEXR'
    'jpeg2000 OPENJPEG'
    'keyring LIBSECRET'
    LUA
    OPENMP
    'providers:graphicsmagick GRAPHICSMAGICK'
    'providers:imagemagick IMAGEMAGICK'
    WEBP
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

darktable_src_prepare() {
    cmake_src_prepare

    # we need to introduce our own build type for rawspeed, otherwise it'll fail
    edo sed -i -e 's:FUZZ:NONE:' "${CMAKE_SOURCE}"/src/external/rawspeed/cmake/build-type.cmake
}

darktable_src_compile() {
    # jsonschema: error: argument -i/--instance: invalid _json_file value
    require_utf8_locale

    default
}

darktable_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

darktable_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

