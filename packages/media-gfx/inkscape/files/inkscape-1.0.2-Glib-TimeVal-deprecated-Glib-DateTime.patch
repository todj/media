Upstream: yes
Reason: Fixes the build

From c719ad24d6a57681566e7751d74829bff19c025f Mon Sep 17 00:00:00 2001
From: Thomas Holder <thomas@thomas-holder.de>
Date: Tue, 17 Mar 2020 21:53:25 +0000
Subject: [PATCH] Glib::TimeVal (deprecated) -> Glib::DateTime

- replace usage of deprecated `TimeVal` with `DateTime`
- Add `-DGLIBMM_DISABLE_DEPRECATED` for strict build mode
---
 CMakeLists.txt          |  2 +-
 src/extension/timer.cpp | 15 ++++++---------
 src/extension/timer.h   |  6 +++---
 3 files changed, 10 insertions(+), 13 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index a7be55d729..eeaeb37d0e 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -82,7 +82,7 @@ endif()
 
 # Define a very strict set of build flags that will prevent any use of deprecated symbols.
 # This will almost certainly cause compilation failure and is intended only for developer use.
-set(CMAKE_CXX_FLAGS_STRICT "${CMAKE_CXX_FLAGS_DEBUG} -Werror=deprecated-declarations -DGTK_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGTKMM_DISABLE_DEPRECATED -DGDKMM_DISABLE_DEPRECATED"
+set(CMAKE_CXX_FLAGS_STRICT "${CMAKE_CXX_FLAGS_DEBUG} -Werror=deprecated-declarations -DGTK_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGTKMM_DISABLE_DEPRECATED -DGDKMM_DISABLE_DEPRECATED -DGLIBMM_DISABLE_DEPRECATED"
 	CACHE STRING
 	"Flags used by C++ compiler during Strict builds"
 	FORCE)
diff --git a/src/extension/timer.cpp b/src/extension/timer.cpp
index 7ed856ec4e..4533b0d85b 100644
--- a/src/extension/timer.cpp
+++ b/src/extension/timer.cpp
@@ -47,8 +47,7 @@ ExpirationTimer::ExpirationTimer (Extension * in_extension):
         timer_list->next = this;
     }
 
-    expiration.assign_current_time();
-    expiration += timeout;
+    expiration = Glib::DateTime::create_now_utc().add_seconds(timeout);
     
     if (!timer_started) {
         Glib::signal_timeout().connect(sigc::ptr_fun(&timer_func), timeout * 1000 / TIMER_SCALE_VALUE);
@@ -106,14 +105,13 @@ ExpirationTimer::~ExpirationTimer()
 void
 ExpirationTimer::touch ()
 {
-    Glib::TimeVal current;
-    current.assign_current_time();
+    auto const current = Glib::DateTime::create_now_utc();
 
-    long time_left = (long)(expiration.as_double() - current.as_double());
+    auto time_left = expiration.difference(current);
     if (time_left < 0) time_left = 0;
     time_left /= 2;
 
-    expiration = current + timeout + time_left;
+    expiration = current.add(time_left).add_seconds(timeout);
     return;
 }
 
@@ -126,9 +124,8 @@ ExpirationTimer::expired () const
 {
     if (locked > 0) return false;
 
-    Glib::TimeVal current;
-    current.assign_current_time();
-    return expiration < current;
+    auto const current = Glib::DateTime::create_now_utc();
+    return expiration.difference(current) < 0;
 }
 
 // int idle_cnt = 0;
diff --git a/src/extension/timer.h b/src/extension/timer.h
index a4f9bfea99..ce9c495571 100644
--- a/src/extension/timer.h
+++ b/src/extension/timer.h
@@ -16,7 +16,7 @@
 
 #include <cstddef>
 #include <sigc++/sigc++.h>
-#include <glibmm/timeval.h>
+#include <glibmm/datetime.h>
 
 namespace Inkscape {
 namespace Extension {
@@ -28,7 +28,7 @@ class ExpirationTimer {
     static ExpirationTimer * timer_list;
     /** \brief Which timer was on top when we started the idle loop */
     static ExpirationTimer * idle_start;
-    /** \brief What the current timeout is */
+    /** \brief What the current timeout is (in seconds) */
     static long timeout;
     /** \brief Has the timer been started? */
     static bool timer_started;
@@ -38,7 +38,7 @@ class ExpirationTimer {
     /** \brief Next entry in the list */
     ExpirationTimer * next;
     /** \brief When this timer expires */
-    Glib::TimeVal expiration;
+    Glib::DateTime expiration;
     /** \brief What extension this function relates to */
     Extension * extension;
 
-- 
2.31.0.rc2

