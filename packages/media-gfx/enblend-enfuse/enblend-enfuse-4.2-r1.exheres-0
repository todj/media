# Copyright 2010 Xavier Barrachina <xabarci@doctor.upv.es>
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz project=enblend ] cmake

SUMMARY="Enblend combines images no seams"
DESCRIPTION="
Enblend combines images that overlap into a single large image with no seams. Enfuse combines
images that overlap into a single image with good exposure and good focus.
"

LICENCES="BSD-3 FDL-1.2 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug       [[ description = [ Turn on debugging ] ]]
    openexr     [[ description = [ Adds support for reading and writing OpenEXR images ] ]]
    openmp      [[ description = [ Parallelize parts of Enblend and Enfuse with OpenMP ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-apps/help2man
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/boost[>=1.55.0]
        dev-libs/vigra[>=1.9.0]
        media-libs/lcms2[>=2.5]
        media-libs/libpng:=
        media-libs/tiff
        sci-libs/gsl
        sys-libs/zlib
        x11-libs/libXi
        openexr? ( media-libs/openexr )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-4.2-add-missing-cmakelists.patch
    "${FILES}"/${PN}-gcc10.patch
)

src_configure() {
    export MANDIR="/usr/share/man"

    local cmakeparams=(
        -DCMAKE_BUILD_TYPE:STRING=$(option debug 'Debug' 'Release')
        -DDOC:BOOL=FALSE
        -DENABLE_TCMALLOC:BOOL=FALSE
        $(cmake_disable_find OpenEXR)
        $(cmake_enable OPENMP)
    )

    ecmake "${cmakeparams[@]}"
}

