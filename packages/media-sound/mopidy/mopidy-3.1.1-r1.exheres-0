# Copyright 2016-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    setup-py [ import=setuptools blacklist='2 3.6' has_bin=true multibuild=false test=pytest ] \
    systemd-service [ systemd_files=[ extra/systemd/mopidy.service ] ]

SUMMARY="An extensible music server with MPD and Spotify support written in Python"

REMOTE_IDS+=" pypi:Mopidy"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# broken, last checked: 3.0.2
RESTRICT="test"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        dev-python/Pykka[>=2.0.1][python_abis:*(-)?]
        dev-python/requests[>=2.0][python_abis:*(-)?]
        dev-python/tornado[>=4.4][python_abis:*(-)?]
        media-libs/gstreamer:1.0[>=1.2.3][gobject-introspection]
        media-plugins/gst-plugins-base:1.0[>=1.2.3][gobject-introspection][gstreamer_plugins:ogg]
        media-plugins/gst-plugins-good:1.0[>=1.2.3][gstreamer_plugins:flac][gstreamer_plugins:soup][gstreamer_plugins:mpg123]
    run:
        gnome-bindings/pygobject:3[python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/responses[python_abis:*(-)?]
    suggestion:
        media-plugins/gst-plugins-base:1.0[gstreamer_plugins:alsa] [[
            description = [ ALSA backend support ]
        ]]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:pulseaudio] [[
            description = [ PulseAudio backend support ]
        ]]
        media-plugins/mopidy-iris[>=3.44.0] [[
            description = [ Mopidy extension providing a Spotify-like webinterface ]
        ]]
        media-plugins/mopidy-local [[
            description = [ Mopidy extension for playing music from your local music archive ]
        ]]
        media-plugins/mopidy-mpd [[
            description = [ Mopidy extension for controlling Mopidy from MPD clients ]
        ]]
        media-plugins/mopidy-musicbox-webclient[>=3.0.0] [[
            description = [ Mopidy extension providing the MusicBox webinterace ]
        ]]
        media-plugins/mopidy-scrobbler[>=2.0.0] [[
            description = [ Mopidy extension for scrobbling played tracks to Last.fm ]
        ]]
        media-plugins/mopidy-spotify[>=4.0.0] [[
            description = [ Mopidy extension for playing music from Spotify ]
        ]]
        media-sound/upmpdcli [[
            description = [ Allows to expose Mopidy as an UPnP MediaRenderer ]
        ]]
        net-apps/snapcast [[
            description = [ Allows to turn Mopidy into a multi-room audio solution ]
        ]]
"

src_install() {
    setup-py_src_install

    dobin extra/mopidyctl/mopidyctl

    doman extra/mopidyctl/mopidyctl.8

    insinto /etc/${PN}
    doins "${FILES}"/{logging,mopidy}.conf
    edo chown -R mopidy:mopidy "${IMAGE}"/etc/${PN}
    edo chmod 0600 "${IMAGE}"/etc/${PN}/{logging,mopidy}.conf

    install_systemd_files

    insinto /usr/share/applications
    doins extra/desktop/${PN}.desktop

    keepdir /var/{cache,lib,log}/mopidy
    keepdir /var/lib/mopidy/{local,media,playlists}
    edo chown -R mopidy:mopidy "${IMAGE}"/var/{cache,lib,log}/mopidy
}

